package ecdsatools

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"
)

//EncodePublicKey encode publicKey and gives the string representation of the pem encoded key
func EncodePublicKey(publicKey *ecdsa.PublicKey) string {
	x509EncodedPub, _ := x509.MarshalPKIXPublicKey(publicKey)
	pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC ECDSA KEY", Bytes: x509EncodedPub})

	return string(pemEncodedPub)
}

//EncodePrivateKey encode privateKey and gives the string representation of the pem encoded key
func EncodePrivateKey(privateKey *ecdsa.PrivateKey) string {
	x509Encoded, _ := x509.MarshalECPrivateKey(privateKey)
	pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE ECDSA KEY", Bytes: x509Encoded})

	return string(pemEncoded)
}

//Encode the keys in pem strings and return them, see Encode<private|public>Key for implementation
//taken from SO https://stackoverflow.com/questions/21322182/how-to-store-ecdsa-private-key-in-go
func Encode(privateKey *ecdsa.PrivateKey, publicKey *ecdsa.PublicKey) (string, string) {
	return EncodePrivateKey(privateKey), EncodePublicKey(publicKey)
}

//DecodePublicKey take a string pemEncoded and output the corresponding public key
func DecodePublicKey(pemEncodedPublicKey string) *ecdsa.PublicKey {
	blockPub, _ := pem.Decode([]byte(pemEncodedPublicKey))
	x509EncodedPub := blockPub.Bytes
	genericPublicKey, _ := x509.ParsePKIXPublicKey(x509EncodedPub)
	publicKey := genericPublicKey.(*ecdsa.PublicKey)

	return publicKey
}

//DecodePrivateKey decode the pemEncoded private key  (string) and return the corresponding privateKey
func DecodePrivateKey(pemEncodedPrivateKey string) *ecdsa.PrivateKey {
	block, _ := pem.Decode([]byte(pemEncodedPrivateKey))
	x509Encoded := block.Bytes
	privateKey, _ := x509.ParseECPrivateKey(x509Encoded)

	return privateKey
}

//Decode two encoded pem strings ecdsa key into a private key and a public key
func Decode(pemEncoded string, pemEncodedPub string) (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	return DecodePrivateKey(pemEncoded), DecodePublicKey(pemEncodedPub)
}

//WriteKeysToDisk as key.pub and key.priv
func WriteKeysToDisk(privKey *ecdsa.PrivateKey, pubKey *ecdsa.PublicKey) {

	WritePrivateKeyToDisk(privKey)
	WritePublicKeyToDisk(pubKey)

}

//WritePublicKeyToDisk write the public key to ./key.pub
func WritePublicKeyToDisk(publicKey *ecdsa.PublicKey) {
	fpub, err := os.Create("./key.pub")
	if err != nil {
		log.Printf("unable to create key.pub \n%v", err)
	}
	fpub.Write([]byte(EncodePublicKey(publicKey)))
}

//WritePrivateKeyToDisk write privateKey to disk as key.priv
func WritePrivateKeyToDisk(privateKey *ecdsa.PrivateKey) {
	fpriv, err := os.Create("./key.priv")
	if err != nil {
		log.Printf("unable to creat ./key.priv \n%v", err)
	}
	fpriv.Write([]byte(EncodePrivateKey(privateKey)))
}

//ReadPrivateKeyFromFile return a private key struct, stored as pem string in filename
func ReadPrivateKeyFromFile(filename string) *ecdsa.PrivateKey {
	fd, err := os.Open(filename)
	if err != nil {
		log.Printf("unable to open file %s", filename)
		return nil
	}

	key, err := ioutil.ReadAll(fd)
	if err != nil {
		log.Printf("unable to read file %s", fd.Name())
		return nil
	}

	return DecodePrivateKey(string(key))
}

//ReadPublicKeyFromFile return a public key struct, stored as pem string in filename
func ReadPublicKeyFromFile(filename string) *ecdsa.PublicKey {
	fd, err := os.Open(filename)
	if err != nil {
		log.Printf("unable to open file %s", filename)
		return nil
	}

	key, err := ioutil.ReadAll(fd)
	if err != nil {
		log.Printf("unable to read file %s", fd.Name())
		return nil
	}

	return DecodePublicKey(string(key))
}

//ReadKeysFromFile return  the private and public keys stored in file indicated by the filename string
func ReadKeysFromFile(privFilename, pubFilename string) (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	return ReadPrivateKeyFromFile(privFilename), ReadPublicKeyFromFile(pubFilename)
}

//Exemple of key generation using p384
// func generateECDSAKeys() (*ecdsa.PrivateKey, error) {
// 	the bigger the number , the best encryption is
// 	cf https://csrc.nist.gov/csrc/media/events/workshop-on-elliptic-curve-cryptography-standards/documents/papers/session6-adalier-mehmet.pdf
// 	return ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
// }
