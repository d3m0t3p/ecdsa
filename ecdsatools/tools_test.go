package ecdsatools

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"reflect"
	"testing"
)

func TestDecodeEncode(t *testing.T) {
	privateKey, _ := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)

	encPriv, encPub := Encode(privateKey, &privateKey.PublicKey)

	priv2, pub2 := Decode(encPriv, encPub)

	if !reflect.DeepEqual(privateKey, priv2) {
		t.Error("Changes were made to the private key while encoding / decoding to pem string")
	}
	if !reflect.DeepEqual(&privateKey.PublicKey, pub2) {
		t.Error("Changes were made to the public key while encoding / decoding to pem string")
	}
	t.Logf("Passed encoding decoding pem string to ecdsa keys")

}
func TestReadWritePemKeys(t *testing.T) {
	pri, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		t.Errorf("unable to generate ecdsa keys %v", err)
	}

	//WriteKeysToDisk as key.pub and key.priv
	WriteKeysToDisk(pri, &pri.PublicKey)

	pri2, pub2 := ReadKeysFromFile("./key.priv", "./key.pub")

	if !reflect.DeepEqual(pri2, pri) {
		t.Error("Changes were made to the private key while writing/reading pem string to/from file ")
	}

	if !reflect.DeepEqual(&pri.PublicKey, pub2) {
		t.Error("Changes were made to the public key while writing/reading pem string to/from file ")

	}
}
