package main

//TODO test intensif of signedMsg and  ecdsa tools
import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"

	"log"

	et "./ecdsatools"
	sm "./signedmsg"
)

func main() {
	//read private key

	privateKey := et.ReadPrivateKeyFromFile("key.priv")
	signedMsg := sm.NewSignedMessage("order 66")

	signedMsg.Sign(privateKey)
	pvK, _ := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	log.Printf("correctly signed : %t", signedMsg.Verify(&pvK.PublicKey))
}
