package signedmsg

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"log"
	"math/big"
)

//SignedMessage is a struct containing everything to verif a message
type SignedMessage struct {
	Msg    string   `json:"msg"`
	MsgSum []byte   `json:"msg_sum"`
	S      *big.Int `json:"s"`
	R      *big.Int `json:"r"`
}

//NewSignedMessage construct a new signed message with msg
func NewSignedMessage(msg string) *SignedMessage {
	hash := sha256.Sum256([]byte(msg))
	return &SignedMessage{Msg: msg, MsgSum: hash[:]}
}

//Sign the message with a private key
func (m *SignedMessage) Sign(privateKey *ecdsa.PrivateKey) {
	if len(m.Msg) == 0 {
		return
	}
	if len(m.MsgSum) == 0 {
		return
	}

	r, s, err := ecdsa.Sign(rand.Reader, privateKey, m.MsgSum[:])
	if err != nil {
		log.Fatalf("coulnd sign message %v", err)
	}

	m.R = r
	m.S = s
}

//Verify check with the public key if the message has been signed with the corresponding private key, if the message hasn't been modified or altered
func (m *SignedMessage) Verify(publicKey *ecdsa.PublicKey) bool {
	return ecdsa.Verify(publicKey, m.MsgSum, m.R, m.S)
}
